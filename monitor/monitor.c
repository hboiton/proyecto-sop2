#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <limits.h>
#include <time.h>
#include <string.h>
 
#define MAX_EVENTS 1024 /*Max. number of events to process at one go*/
#define LEN_NAME 16 /*Assuming that the length of the filename won't exceed 16 bytes*/
#define EVENT_SIZE  ( sizeof (struct inotify_event) ) /*size of one event*/
#define BUF_LEN     ( MAX_EVENTS * ( EVENT_SIZE + LEN_NAME )) /*buffer to store the data of events*/

char horaevento [100];
time_t t;
struct tm *tm;
char hora [200]="en la hora :";
FILE *ptrLog;

void agregarEvento(char *evento, char *nombre , char *hora ){
    //ptrLog es donde vamos a guargar el log
    ptrLog = fopen("/home/hugo201213192/proyecto-sop2/monitor/201213192_files.log", "a+");
    if(ptrLog ==  NULL)
        perror("No se pudo abrir el archivo log en /home/hugo201213192/proyecto-sop2/monitor/201213192_files.log");
    fprintf(ptrLog, "%s: %s %s\n", evento, nombre,hora);
    fclose(ptrLog);
}
 
int main( int argc, char **argv ){
    int length, i = 0, wd;
    int fd;
    char buffer[BUF_LEN];
 
    fd = inotify_init();
    t=time(0);
    tm=localtime(&t);
    if ( fd < 0 ) {
        perror( "No se pudo inicializar Inotify");
    }
    // para monitoriar en el proyecto debe ser la  carpeta /
    wd = inotify_add_watch(fd, "/", IN_CREATE | IN_MODIFY | IN_DELETE); 
    strftime(horaevento, 100, "%d/%m/%Y %H:%M:%S", tm);
    strcpy(hora,"en la hora :");
    strcat(hora,horaevento);    
    
    if (wd == -1){
      printf("No se pudo monitorear %s\n","/");
    }else{
      printf("Monitoreando:: %s %s\n","/",hora);
      agregarEvento("Monitoreando", "/",hora);
    }
 
    while(1){
        i = 0;
        length = read(fd, buffer, BUF_LEN );  
 
        if ( length < 0 ) {
            perror( "error de lectura" );
        }  
 
        while ( i < length ) {
            struct inotify_event *event = ( struct inotify_event * ) &buffer[ i ];

            if ( event->len ) {
                t=time(0);
                tm=localtime(&t);
                strftime(horaevento, 100, "%d/%m/%Y %H:%M:%S", tm);
                strcpy(hora,"en la hora :");
                strcat(hora,horaevento);
                if ( event->mask & IN_CREATE) {
                    if (event->mask & IN_ISDIR){

                      printf( "Se creo el directorio: %s en la hora: %s\n" , event->name,horaevento );       
                      agregarEvento("Se creo el directorio", event->name,hora);
                    }
                    else{
                      printf( "Se creo el archivo: %s en la hora: %s\n", event->name, horaevento );       
                      agregarEvento("Se creo el archivo", event->name,hora);
                    }
                }
           
                if ( event->mask & IN_MODIFY) {
                    if (event->mask & IN_ISDIR){
                        printf( "Se modifico el directorio: %s en la hora: %s\n", event->name,horaevento);       
                        agregarEvento("Se modifico el directorio", event->name,hora);
                    }else{
                        printf( "Se modifico el archivo: %s en la hora: %s\n", event->name, horaevento );       
                        agregarEvento("Se modifico el archivo", event->name,hora);
                    }
                }
           
                if ( event->mask & IN_DELETE) {
                    if (event->mask & IN_ISDIR){
                        printf( "Se elimino el directorio: %s en la hora: %s\n", event->name,horaevento);       
                        agregarEvento("Se elimino el directorio", event->name,hora);
                    }else{
                        printf( "Se elimino el archivo: %s en la hora: %s\n", event->name, horaevento );       
                        agregarEvento("Se elimino el archivo", event->name,hora);
                    }
                }  

                i += EVENT_SIZE + event->len;
            }
        }
    }
 
    inotify_rm_watch( fd, wd );
    close( fd );
   
    return 0;
}